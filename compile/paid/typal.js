import { makeCalendar } from './compile/calendar' /* compiler renameReport:../rename-report.txt */
require('./types/typedefs')

/** @api {eco.artd.calendar.makeCalendar} */
export default makeCalendar
