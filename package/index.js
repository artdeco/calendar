import { license, LOCAL, LUDDITES as luddites, removeTypalMeta } from '@artdeco/package'
import '@luddites/lud'
import { description, name as _name } from '../package.json'
import { TYPEDEFS } from './common'
import { LIB } from './LIB.env'
import { HEADERS } from './HEADERS.env'

export { LIB, HEADERS }

export let name = _name

export let keywords = ['artdeco','calendar']

export let author = 'Art Deco™<packages@artdeco.software>'
export let bugs = {
  url: 'https://artdeco.software/issues/',
  email: 'issues+eco.artd.calendar+calendar@artdeco.software',
}
export let homepage = 'https://artdeco.software/eco.artd.calendar/calendar/'

export { license }

export const LUDDITES = luddites(description)
LUDDITES.copy = { ...LUDDITES.copy, ...TYPEDEFS }
LUDDITES.postProcessing = () => {
  removeTypalMeta('package/luddites', 'types/typedefs.js')
}
LOCAL.copy = { ...LOCAL.copy, ...TYPEDEFS }

export { LOCAL }
LOCAL.postProcessing = () => {
  removeTypalMeta('compile/paid', 'types/typedefs.js')
}