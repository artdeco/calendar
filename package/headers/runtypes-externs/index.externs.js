/**
 * @fileoverview
 * @externs
 */



/** @record */
eco.artd.calendar.makeCalendar.Options = function() {}
/** @type {(!Date)|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.today
/** @type {number|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.year
/** @type {number|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.month
/** @type {boolean|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.disablePast
/** @type {number|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.timezoneOffset
/** @type {boolean|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.generatePrev
/** @type {boolean|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.generateNext

/** @typedef {{ day: number, date: Date, isPast: boolean, isDisabled: (boolean|undefined), isPrevMonth: (boolean|undefined), isNextMonth: (boolean|undefined) }} */
eco.artd.calendar.CalendarCell

