/** @nocompile */
/** @const */
var eco = {}
eco.artd = {}
eco.artd.calendar = {}

/** @typedef {typeof eco.artd.calendar.makeCalendar} */
/**
 * Creates a 2D array with dates for the given month.
 * @param {!eco.artd.calendar.makeCalendar.Options} [options] Optional data to generate dates.
 * - `[today]` _!Date?_ The today's date to calculate whether calendar dates are in the past. By
 * default, makes a `new Date()`.
 * - `[year]` _number?_ The year, by default takes the full year of the today's date.
 * - `[month]` _number?_ The month, by default takes the month of the today's date.
 * - `[disablePast=false]` _boolean?_ Whether to include the `isDisabled` property of the cell for dates which
 * have passed relative to the date from the `today` field. Default `false`.
 * - `[timezoneOffset]` _number?_ The timezone offset. By default, uses the time timezone offset of the
 * `today`'s date: `today.getTimezoneOffset() / 60`. When `0` is passed, a
 * UTC time is used which is good for testing, but locale-specifics need to
 * be considered for business purposes.
 * - `[generatePrev=true]` _boolean?_ Whether to include cells from the previous month. Default `true`.
 * - `[generateNext=true]` _boolean?_ Whether to include cells from the next month. Default `true`.
 * @return {!Array<!Array<!eco.artd.calendar.CalendarCell>>} A 2D array with calendar cells.
 */
eco.artd.calendar.makeCalendar = function(options) {}

