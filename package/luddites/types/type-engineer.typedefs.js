/** @nocompile */
/** @const */
var engineering = {}
engineering.type = {}

/** @typedef {function(new: engineering.type.IInitialiserCaster<ITEM, PROPS>)} engineering.type.IInitialiser.constructor */
/**
 * @interface engineering.type.IInitialiser
 * @template ITEM
 * @template PROPS
 */
engineering.type.IInitialiser = class extends /** @type {engineering.type.IInitialiser.constructor} */ (class {}) {
  /**
   * Since the types now enjoy horizontal inheritance, they need a better
   * initialisation strategy instead of the constructor method. This is
   * a general signature that applies to all protypes.
   * @param {PROPS} [props] Properties to initialise the instance with.
   * @param {function(ITEM, PROPS): void} [callback] The function to apply upon successful initialisation.
   * @return The initialised item for chaining.
   */
  initialiser(props, callback) {
    return /** @type {ITEM} */ (void 0)
  }
}
engineering.type.IInitialiser.prototype.constructor = engineering.type.IInitialiser

/**
 * A concrete class of _IInitialiser_ instances.
 * @constructor engineering.type.Initialiser
 * @implements {engineering.type.IInitialiser<ITEM, PROPS>} ‎
 * @template ITEM
 * @template PROPS
 * @extends {engineering.type.IInitialiser<ITEM,PROPS>}
 */
engineering.type.Initialiser = class extends engineering.type.IInitialiser { }
engineering.type.Initialiser.prototype.constructor = engineering.type.Initialiser

/** @typedef {engineering.type.IInitialiser<ITEM, PROPS>} */
engineering.type.RecordIInitialiser

/**
 * @typedef {engineering.type.IInitialiser<ITEM, PROPS>} engineering.type.BoundIInitialiser<ITEM, PROPS>
 * @template ITEM
 * @template PROPS
 */

/**
 * @typedef {engineering.type.Initialiser<ITEM, PROPS>} engineering.type.BoundInitialiser<ITEM, PROPS>
 * @template ITEM
 * @template PROPS
 */

/** @typedef {typeof engineering.type.IInitialiser.initialiser} */
/**
 * Since the types now enjoy horizontal inheritance, they need a better
 * initialisation strategy instead of the constructor method. This is
 * a general signature that applies to all protypes.
 * @param {PROPS} [props] Properties to initialise the instance with.
 * @param {function(ITEM, PROPS): void} [callback] The function to apply upon successful initialisation.
 * @return {ITEM} The initialised item for chaining.
 * @template ITEM
 * @template PROPS
 */
engineering.type.IInitialiser.initialiser = function(props, callback) {}

/** @typedef {function(new: engineering.type.IEngineerCaster)} engineering.type.IEngineer.constructor */
/**
 * This class provides methods that are used at runtime to interact with the type
 * system of objects.
 * @interface engineering.type.IEngineer
 */
engineering.type.IEngineer = class extends /** @type {engineering.type.IEngineer.constructor} */ (class {}) { }
/** @type {typeof engineering.type.IEngineer.__$extend} */
engineering.type.IEngineer.prototype.__$extend = function() {}
/** @type {typeof engineering.type.IEngineer.__$implements} */
engineering.type.IEngineer.prototype.__$implements = function() {}
/** @type {typeof engineering.type.IEngineer.__$constructor} */
engineering.type.IEngineer.prototype.__$constructor = function() {}

/**
 * A concrete class of _IEngineer_ instances.
 * @constructor engineering.type.Engineer
 * @implements {engineering.type.IEngineer} This class provides methods that are used at runtime to interact with the type
 * system of objects.
 */
engineering.type.Engineer = class extends engineering.type.IEngineer { }
engineering.type.Engineer.prototype.constructor = engineering.type.Engineer
/**
 * Adds implementations to the abstract class.
 * @param {...!(engineering.type.IEngineer|typeof engineering.type.Engineer)} Implementations
 */
engineering.type.Engineer.__implement = function(...Implementations) {}

/** @typedef {engineering.type.IEngineer} */
engineering.type.RecordIEngineer

/** @typedef {engineering.type.IEngineer} engineering.type.BoundIEngineer */

/** @typedef {engineering.type.Engineer} engineering.type.BoundEngineer */

/** @typedef {typeof engineering.type.IEngineer.__$extend} */
/**
 * Extends an existing instance (not its prototype) with methods, fields and accessors from the target instance and copies across all values from the target.
 * Allows to enhance objects with dynamic behaviour at runtime.
 * @param {(function(new: ?, ...?)|!Object)} protype The constructor or protype used to copy the property descriptors from. This argument
 * will be passed to the `subtype` method of the `@protypes/protypes` package.
 * @param {...?} target Initialised instances of the target protype to copy values from.
 * @return {void}
 */
engineering.type.IEngineer.__$extend = function(protype, ...target) {}

/** @typedef {typeof engineering.type.IEngineer.__$implements} */
/**
 * Checks if the instance implements the constructor by its name.
 * Corresponds to the `isa` message in Obj C.
 * @param {string} className The class name to check for.
 * @return {boolean} Whether the protype with the required name participates in formation of instance's prototype.
 */
engineering.type.IEngineer.__$implements = function(className) {}

/** @typedef {typeof engineering.type.IEngineer.__$constructor} */
/**
 * This service constructor will include auto-generated assignments to fields
 * with initial values. It is automatically invoked from the main constructor
 * method. Its advantage in the ability to combine multiple `__$constructors`
 * from different implementations.
 * @param {...?} args Any arguments passed to the actual constructor.
 * @return {void}
 */
engineering.type.IEngineer.__$constructor = function(...args) {}

/** @typedef {function(new: engineering.type.IEngineer&engineering.type.IAbstractClassCaster)} engineering.type.IAbstractClass.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * An abstract class generated with _Typal_. It'll have the static `implement`
 * method that can be called to trigger the subtyping.
 * @interface engineering.type.IAbstractClass
 */
engineering.type.IAbstractClass = class extends /** @type {engineering.type.IAbstractClass.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
engineering.type.IAbstractClass.prototype.constructor = engineering.type.IAbstractClass

/**
 * A concrete class of _IAbstractClass_ instances.
 * @constructor engineering.type.AbstractClass
 * @implements {engineering.type.IAbstractClass} An abstract class generated with _Typal_. It'll have the static `implement`
 * method that can be called to trigger the subtyping.
 */
engineering.type.AbstractClass = class extends engineering.type.IAbstractClass { }
engineering.type.AbstractClass.prototype.constructor = engineering.type.AbstractClass
/**
 * Adds implementations to the abstract class.
 * @param {...(!(engineering.type.IAbstractClass|typeof engineering.type.AbstractClass)|(engineering.type.IEngineer|typeof engineering.type.Engineer))} Implementations
 */
engineering.type.AbstractClass.__implement = function(...Implementations) {}

/** @typedef {engineering.type.IAbstractClass} */
engineering.type.RecordIAbstractClass

/** @typedef {engineering.type.IAbstractClass} engineering.type.BoundIAbstractClass */

/** @typedef {engineering.type.AbstractClass} engineering.type.BoundAbstractClass */

/** @typedef {typeof engineering.type.IAbstractClass.__implement} */
/**
 * This method is generated by the tool and will be specific to each abstract
 * class by typing the implementation types, to enable type-checking on
 * parameters passed to the method.
 * @return {void}
 * @example
 * ```js
 * import {
 * ‎  subtype, assignSupers, time, SUBTYPE_OPTS, implementEngineer,
 * } from '＠type.engineering/type-engineer'
 *
 * class _AbstractDBPrompter {}
 * /**
 * ‎ * A prompter that can save cards in the database.
 * ‎ * ＠abstract
 * ‎ * ＠implements {_mlCard.mlteam.IDBPrompter}
 * ‎ * ＠extends {_mlCard.mlteam.DBPrompter}
 * ‎ *‎/
 * ```
 * ```js
 * export class AbstractDBPrompter extends _AbstractDBPrompter {
 * ‎  /**
 * ‎   * Sets the implementation on the interface.
 * ‎   * ＠param {...!(typeof AbstractDBPrompter|AbstractDBPrompter)} Implementations
 * ‎   * ＠suppress {checkPrototypalTypes}
 * ‎   *‎/
 * ```
 * ```js
 * ‎  static __implement(...Implementations) {
 * ‎    const start = Date.now()
 * ‎    const s = subtype(_AbstractDBPrompter, SUBTYPE_OPTS, ...Implementations)
 * ‎    assignSupers(_AbstractDBPrompter, Implementations)
 * ‎    time(_AbstractDBPrompter, Date.now() - start)
 * ‎    return s
 * ‎  }
 * }
 * implementEngineer(AbstractDBPrompter)
 *
 * ```
 */
engineering.type.IAbstractClass.__implement = function() {}

/**
 * Contains getters to cast the _IInitialiser_ interface.
 * @interface engineering.type.IInitialiserCaster
 * @template ITEM
 * @template PROPS
 */
engineering.type.IInitialiserCaster = class {
  constructor() {
    /**
     * Cast the _IInitialiser_ instance into the _BoundIInitialiser_ type.
     * @type {!engineering.type.BoundIInitialiser<ITEM, PROPS>}
     */
    this.asIInitialiser
    /**
     * Access the _Initialiser_ prototype.
     * @type {!engineering.type.BoundInitialiser<ITEM, PROPS>}
     */
    this.superInitialiser
  }
}
engineering.type.IInitialiserCaster.prototype.constructor = engineering.type.IInitialiserCaster

/**
 * Contains getters to cast the _IEngineer_ interface.
 * @interface engineering.type.IEngineerCaster
 */
engineering.type.IEngineerCaster = class { }
/**
 * Cast the _IEngineer_ instance into the _BoundIEngineer_ type.
 * @type {!engineering.type.BoundIEngineer}
 */
engineering.type.IEngineerCaster.prototype.asIEngineer
/**
 * Access the _Engineer_ prototype.
 * @type {!engineering.type.BoundEngineer}
 */
engineering.type.IEngineerCaster.prototype.superEngineer

/**
 * Contains getters to cast the _IAbstractClass_ interface.
 * @interface engineering.type.IAbstractClassCaster
 */
engineering.type.IAbstractClassCaster = class { }
/**
 * Cast the _IAbstractClass_ instance into the _BoundIAbstractClass_ type.
 * @type {!engineering.type.BoundIAbstractClass}
 */
engineering.type.IAbstractClassCaster.prototype.asIAbstractClass
/**
 * Access the _AbstractClass_ prototype.
 * @type {!engineering.type.BoundAbstractClass}
 */
engineering.type.IAbstractClassCaster.prototype.superAbstractClass

/**
 * @typedef {Object} engineering.type.ET Shorthands for argument checking to save bytes.
 * @prop {string} [S="'string'"] Default `'string'`.
 * @prop {string} [B="'boolean'"] Default `'boolean'`.
 * @prop {string} [N="'number'"] Default `'number'`.
 */

/**
 * @typedef {Object} $engineering.type.SUBTYPE_OPTS
 * @prop {boolean} [setProtypesPrototype=true] Default `true`.
 * @prop {boolean} [mergeGettersSetters=true] Default `true`.
 * @prop {boolean} [overrideTarget=true] Default `true`.
 */
/** @typedef {$engineering.type.SUBTYPE_OPTS&_protypes.Options} engineering.type.SUBTYPE_OPTS Subtyping options configured for type engineering. */

