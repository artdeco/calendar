/** @nocompile */


/**
 * @typedef {Object} eco.artd.calendar.makeCalendar.Options Optional data to generate dates.
 * @prop {!Date} [today] The today's date to calculate whether calendar dates are in the past. By
 * default, makes a `new Date()`.
 * @prop {number} [year] The year, by default takes the full year of the today's date.
 * @prop {number} [month] The month, by default takes the month of the today's date.
 * @prop {boolean} [disablePast=false] Whether to include the `isDisabled` property of the cell for dates which
 * have passed relative to the date from the `today` field. Default `false`.
 * @prop {number} [timezoneOffset] The timezone offset. By default, uses the time timezone offset of the
 * `today`'s date: `today.getTimezoneOffset() / 60`. When `0` is passed, a
 * UTC time is used which is good for testing, but locale-specifics need to
 * be considered for business purposes.
 * @prop {boolean} [generatePrev=true] Whether to include cells from the previous month. Default `true`.
 * @prop {boolean} [generateNext=true] Whether to include cells from the next month. Default `true`.
 */

/**
 * @typedef {Object} eco.artd.calendar.CalendarCell A cell in the calendar field.
 * @prop {number} day A day-of-month, e.g., 1-31.
 * @prop {Date} date A generated date for the cell.
 * @prop {boolean} isPast Whether the cell is in the past relative to today's date.
 * @prop {boolean} [isDisabled] Whether the cell is disabled because it is in the past.
 * @prop {boolean} [isPrevMonth] Whether the cell belongs to the previous month.
 * @prop {boolean} [isNextMonth] Whether the cell belongs to the next month.
 */

