import { removeTypalMeta } from '@artdeco/package'
import '@luddites/lud'
import { readFileSync, writeFileSync } from 'fs'
import { description, name } from '../package.json'

/** @type {_lud.Env} */
export const HEADERS = {
  name: name + '.h',
  ignore: [
    'types/typedefs/api.typal.js',
    'types/typedefs/private/typal.js',
    'types/typedefs/typal.js',
    'types/runtypes-externs/typal.js',
    'types/runtypes-externs/api.typal.js',
    'types/runtypes-externs/index.externs.typal.js',
    'types/runtypes-externs/symbols.typal.js',
  ],
  module: '_.js',
  description: `${description} [HEADERS]`,
  runtypes: [
    'runtypes-externs/ns/$eco.ns',
    'runtypes-externs/ns/$eco.artd.ns',
    'runtypes-externs/ns/$eco.artd.calendar.ns',
    'runtypes-externs/ns/__$te.ns',
    'runtypes-externs/index.js',
    'runtypes-externs/api.js',
    'runtypes-externs/define.js',
    'runtypes-externs/symbols.js',
  ],
  Externs: [
    'runtypes-externs/index.externs.js',
  ],
  externs: [
    'runtypes-externs/ns/eco.artd.calendar.js',
    'runtypes-externs/externs.js',
  ],
  addFiles: [
    'types/typedefs',
    'types/runtypes-externs',

    'types/index.js',
    'node_modules/@artdeco/license/EULA.md',
    'CHANGELOG.md',
  ],
}

HEADERS.postProcessing = async () => {
  console.log('postprocessing types')
  renameVendor('headers')

  writeFileSync('package/headers/_.js', '') // module isn't required
  removeTypalMeta('package/headers', 'typedefs/api.js', 'typedefs/index.js', 'runtypes-externs/api.js',
    'runtypes-externs/index.externs.js','runtypes-externs/index.js','runtypes-externs/symbols.js',)

  const TYPES_PATH = 'package/headers/index.js'
  let r = readFileSync(TYPES_PATH, 'utf-8')
  r = r.replace(/import ('.+')/g, 'require($1)')
  writeFileSync(TYPES_PATH, r)
}


const renameVendor = (pckg) => {
  const VENDOR = `package/${pckg}/typedefs/vendor.js`
  let r = readFileSync(VENDOR, 'utf-8')
  r = r.replace(/import/g, '// import')
  writeFileSync(VENDOR, r)
}