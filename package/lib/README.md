<div align="center"><p align="center">

# @artdeco/calendar

[![npm version](https://badge.fury.io/js/@artdeco/calendar.svg)](https://www.npmjs.com/package/@artdeco/calendar)
[![Pipeline Badge](https://gitlab.com/artdeco/calendar/nodejs/badges/master/pipeline.svg)](https://gitlab.com/artdeco/calendar/nodejs/-/commits/master)
</p></div>

`@artdeco/calendar` is: Generates A 2D Calendar Array For The Given Month Of Year.

```sh
yarn add @artdeco/calendar
npm i @artdeco/calendar
```

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [API](#api)
- [`async calendar(config: !CalendarConfig): !Promise<string>`](#async-mynewpackageconfig-mynewpackageconfig-promisestring)
  * [`CalendarConfig`](#type-mynewpackageconfig)
- [`Calendar`](#type-mynewpackage)
- [CLI](#cli)
- [Copyright & License](#copyright--license)

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/calendar/nodejs/section-breaks/0.svg">
</a></p></div>

## API

The package is available by importing its default function and named class:

```js
import calendar, { Calendar } from '@artdeco/calendar'
```

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/calendar/nodejs/section-breaks/1.svg">
</a></p></div>

## <code>async <ins>calendar</ins>(</code><sub><br/>&nbsp;&nbsp;`config: !CalendarConfig,`<br/></sub><code>): <i>!Promise&lt;string&gt;</i></code>

Generates A 2D Calendar Array For The Given Month Of Year.

 - <kbd><strong>config*</strong></kbd> <em><code>![CalendarConfig](#type-mynewpackageconfig "Additional options for the program.")</code></em>: Additional options for the program.

__<a name="type-mynewpackageconfig">`CalendarConfig`</a>__: Additional options for the program.

|   Name    |   Type    |    Description    | Default |
| --------- | --------- | ----------------- | ------- |
| shouldRun | _boolean_ | A boolean option. | `true`  |
| text      | _string_  | A text to return. | -       |

```js
import calendar from '@artdeco/calendar'

(async () => {
  const res = await calendar({
    text: 'example',
  })
  console.log(res)
})()
```
```
@artdeco/calendar called with example
example
```

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/calendar/nodejs/section-breaks/2.svg">
</a></p></div>

__<a name="type-mynewpackage">`Calendar`</a>__: A representation of a package.

|      Name       |                                                               Type                                                               |             Description             | Initial |
| --------------- | -------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------- | ------- |
| __constructor__ | _new () =&gt; [Calendar](#type-mynewpackage "A representation of a package.")_                                               | Constructor method.                 | -       |
| __example__     | _string_                                                                                                                         | An example property.                | `ok`    |
| __run__         | _(conf: &#33;[CalendarConfig](#type-mynewpackageconfig "Additional options for the program.")) =&gt; !Promise&lt;string&gt;_ | Executes main method via the class. | -       |

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/calendar/nodejs/section-breaks/3.svg">
</a></p></div>

## Copyright & License

SEE LICENSE IN LICENSE

<table>
  <tr>
    <td><img src="https://avatars3.githubusercontent.com/u/38815725?v=4&amp;s=100" alt="artdeco"></td>
    <td>© <a href="https://artd.eco">Art Deco™</a> 2020</td>
  </tr>
</table>

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/calendar/nodejs/section-breaks/-1.svg">
</a></p></div>