import run from './run'
import AbstractCalendar from '../../types/lux'

export default class Calendar extends AbstractCalendar {
  constructor() {
    super()
  }
}

/** @constructor @extends {Calendar} @suppress {checkTypes} */ function _Calendar() {}
AbstractCalendar.__implement(Calendar, _Calendar.prototype = /** @type {!Calendar} */ ({
  async run(conf) {
    return await run(conf)
  },
}))