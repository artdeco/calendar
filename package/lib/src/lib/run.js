import { c } from '@artdeco/erte'
import '../../types'

/** @type {eco.artd.calendar.calendar} */
export default async function calendar(config = {}) {
  const {
    shouldRun = true,
    text = '',
  } = config
  if (!shouldRun) return ''
  console.log('@artdeco/calendar called with %s', c(text, 'yellow'))
  return text
}