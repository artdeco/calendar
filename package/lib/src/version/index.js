/**
 * @suppress {uselessCode}
 */
export const isDemoVersino = () => { try {
  return DEMO_VERSION
} catch (err) {
  return false
}}
