## 2 September 2021

### [0.2.0](https://gitlab.com/artdeco/@artdeco/calendar/compare/v0.0.0-pre...v0.2.0)

- [Fix] Fix edge case in certain months.
- [Package] Export the `browser` field in the lib.
- [Package] Publish package on the _Luddites_ registry.

### 0.0.0-pre

- Create `@artdeco/calendar` with _[`NodeTools`](https://art-deco.github.io/nodetools)_.