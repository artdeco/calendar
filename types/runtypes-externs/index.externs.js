/**
 * @fileoverview
 * @externs
 */



/* @typal-type {types/api.xml} eco.artd.calendar.makeCalendar.Options no-functions 7ef91065e8e6ba14fee5b97018c9e851 */
/** @record */
eco.artd.calendar.makeCalendar.Options = function() {}
/** @type {(!Date)|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.today
/** @type {number|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.year
/** @type {number|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.month
/** @type {boolean|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.disablePast
/** @type {number|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.timezoneOffset
/** @type {boolean|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.generatePrev
/** @type {boolean|undefined} */
eco.artd.calendar.makeCalendar.Options.prototype.generateNext

// nss:eco.artd.calendar
/* @typal-end */
/* @typal-type {types/api.xml} eco.artd.calendar.CalendarCell no-functions 7ef91065e8e6ba14fee5b97018c9e851 */
/** @typedef {{ day: number, date: Date, isPast: boolean, isDisabled: (boolean|undefined), isPrevMonth: (boolean|undefined), isNextMonth: (boolean|undefined) }} */
eco.artd.calendar.CalendarCell

// nss:eco.artd.calendar
/* @typal-end */
