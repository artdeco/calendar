/* @typal-type {types/api.xml} eco.artd.calendar.makeCalendar functions 7ef91065e8e6ba14fee5b97018c9e851 */
/**
 * @param {!eco.artd.calendar.makeCalendar.Options} [options]
 * @return {!Array<!Array<!eco.artd.calendar.CalendarCell>>}
 */
$eco.artd.calendar.makeCalendar = function(options) {}
/** @typedef {typeof $eco.artd.calendar.makeCalendar} */
eco.artd.calendar.makeCalendar

// nss:eco.artd.calendar,$eco.artd.calendar
/* @typal-end */
