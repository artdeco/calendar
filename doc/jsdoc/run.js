/* start example */
import { Calendar } from '../../'

/* end example */
;(async () => {
  /* start example */
  let calendar = new Calendar()
  let res = await calendar.run({
    shouldRun: true,
    text: 'hello-world',
  })
  console.log(res) // @artdeco/calendar called with hello-world
  /* end example */
})()