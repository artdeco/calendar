<public gitlab dir="doc/pub" />
<example-override from="src" to="@artdeco/calendar" />
<div align="center"><p align="center">

# @artdeco/calendar

%NPM: @artdeco/calendar%
<pipeline-badge/>
</p></div>

`@artdeco/calendar` is: Generates A 2D Calendar Array For The Given Month Of Year.

```sh
yarn add @artdeco/calendar
npm i @artdeco/calendar
```

## Table Of Contents

%TOC%

%~%