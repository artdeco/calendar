## API

The package is available by importing its default function and named class:

```js
import calendar, { Calendar } from '@artdeco/calendar'
```

<include-types>types/api.xml</include-types>

%~ width="20"%

<function noArgTypesInToc level="3" name="calendar"/>

<example src="doc/example" />
<fork>doc/example</fork>

%~%

<typedef level="2" name="Calendar">types/index.xml</typedef>

%~%