import { deepEqual as equal } from '@type.engineering/web-computing'
import Context from '../context'
import Calendar from '../../src/lib/Calendar'

/** @type {TestSuite} */
const T = {
  context: Context,
  async'gets a link to the fixture'({ fixture, readFile }) {
    const m = new Calendar()
    await m.run()
  },
}

export default T