import { deepEqual as equal } from '@type.engineering/web-computing'
import Context from '../context'
import makeCalendar from '../../src'

/** @type {TestSuite} */
const T = {
  context: Context,
  'is a function'() {
    equal(typeof makeCalendar, 'function')
  },
  'generates a calendar'() {
    const res = makeCalendar({
      today: new Date(2020, 10, 11),
      month: 10,
      year: 2020,
      timezoneOffset: 0,
    })
    return JSON.parse(JSON.stringify(res))
  },
  'generates a calendar 2'() {
    const res = makeCalendar({
      today: new Date(2020, 10, 11),
      month: 9,
      year: 2021,
      timezoneOffset: 0,
    })
    return JSON.parse(JSON.stringify(res))
  },
  'generates a calendar without prev and next'() {
    const res = makeCalendar({
      today: new Date(2020, 10, 11),
      month: 10,
      year: 2020,
      timezoneOffset: 0,
      generateNext: false,
      generatePrev: false,
    })
    return JSON.parse(JSON.stringify(res))
  },
  // async'gets a link to the fixture'({ fixture, readFile }) {
  //   const path = fixture`test.txt`
  //   const text = readFile(path)
  //   const res = await calendar({
  //     text,
  //   })
  //   equal(res, text)
  // },
}

export default T