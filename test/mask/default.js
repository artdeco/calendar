import { makeTestSuite } from '@type.engineering/web-computing'
import { EOL } from 'os'
import Context from '../context'
import calendar from '../../src'

// export default
makeTestSuite('test/result/default', {
  /**
   * @param {Context} ctx
   */
  async getResults({ fixture, readFile }) {
    const text = readFile(fixture`${this.input}.txt`)
    const res = await calendar({
      text,
    })
    return `${this.input}:${EOL}${res}`
  },
  context: Context,
})