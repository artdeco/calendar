import '../../types'

/**
 *
 * @param {number} year
 * @param {number} month
 * @param {number} day
 * @param {number} timezone
 */
const makeDate = (year, month, day, timezone) => {
  const d = Date.UTC(year, month, day, timezone)
  const dd = new Date(d)
  return dd
}

/** @type {eco.artd.calendar.makeCalendar} */
const makeCalendar = ({
  today = new Date(),
  year = today.getFullYear(),
  month = today.getMonth(),
  disablePast = false,
  timezoneOffset = today.getTimezoneOffset() / 60,
  generateNext = true,
  generatePrev = true,
} = {}) => {
  const TODAY_DATE = today.getTime()
  let i = 1
  let date = makeDate(year,month,i, timezoneOffset) // new Date(year, month, i)
  const m = date.getMonth()
  /** @type {!Array<!Array<!eco.artd.calendar.CalendarCell>>} */
  const cal = []
  let row = null
  while (date.getMonth() == m) {
    if (!row) row = Array.from({ length: 7 }).map(() => null)
    const{d,cell}=makeCell(date,TODAY_DATE,disablePast)
    row[d] = cell
    if (d == 6) {
      cal.push(row)
      row = null
    }
    i++
    date = makeDate(year,month,i,timezoneOffset)
  }
  if (row) cal.push(row)
  if (generatePrev) {
    const [firstRow] = cal
    let firstIndex = firstRow.findIndex(v => !!v)
    let j = 0
    while (firstIndex > 0) {
      let d = makeDate(year,month,j,timezoneOffset)
      const{cell}=makeCell(d,TODAY_DATE,disablePast)
      cell.isPrevMonth=true
      j--
      firstIndex--
      firstRow[firstIndex] = cell
    }
  }
  if (generateNext) {
    const lastRow = cal[cal.length - 1]
    let lastIndex = lastRow.findIndex(v=>!v)
    if (lastIndex > -1)  {
      let j = lastRow[lastIndex-1].day + 1
      while (lastIndex < 7) {
        const d = makeDate(year,month,j,timezoneOffset)
        const{cell}=makeCell(d,TODAY_DATE,disablePast)
        cell.isNextMonth = true
        j++
        lastRow[lastIndex]=cell
        lastIndex++
      }
    }
  }
  return cal
}

/**
 * Makes a calendar cell.
 * @param {!Date} date
 * @param {number} TODAY_DATE
 */
const makeCell = (date, TODAY_DATE, disablePast) => {
  const d = (date.getDay() || 7) - 1
  const day = date.getDate()
  const isPast = date.getTime() < TODAY_DATE
  const cell = /** @type {!eco.artd.calendar.CalendarCell} */ ({
    date, day, isPast,
  })
  const isDisabled = disablePast && isPast
  if (isDisabled) cell.isDisabled = true
  return { cell, d }
}

export default makeCalendar